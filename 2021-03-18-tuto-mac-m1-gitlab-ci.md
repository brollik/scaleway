---
layout: docs
title: How to setup GitLab CI for your iOS and macOS projects
description: This page shows you how to setup GitLab CI for your iOS and macOS projects using a hosted Mac mini M1.
tags: compute image baremetal bare metal bmaas macos mac macmini m1 mac mini dedicated git gitlab runner cicd ci
image: /assets/images/docs/scaleway.png
excerpt: |
  add excerpt
totalTime: PT20M
steps:
  - step: Deploying the Mac mini M1
    type: HowToStep
    url: https://www.scaleway.com/en/docs/setting-up-gitlab-ci-for-ios-and-macos-projects/#-Deploying-the-Mac-mini-M1
  - step: Setting-up the Homebrew package manager
    type: HowToStep
    url: https://www.scaleway.com/en/docs/setting-up-gitlab-ci-for-ios-and-macos-projects/#-Settingup-the-Homebrew-package-manager
  - step: Installing the Gitlab Runner
    type: HowToStep
    url: https://www.scaleway.com/en/docs/setting-up-gitlab-ci-for-ios-and-macos-projects/#-Installing-the-Gitlab-Runner
  - step: Configuring the Runner in Gitlab
    type: HowToStep
    url: https://www.scaleway.com/en/docs/setting-up-gitlab-ci-for-ios-and-macos-projects/#-Configuring-the-Runner-in-Gitlab
  - step: Configuring CI for your project
    type: HowToStep
    url: https://www.scaleway.com/en/docs/setting-up-gitlab-ci-for-ios-and-macos-projects/#-Configuring-CI-for-your-project
categories: 
  - compute
brands: 
  - elements
permalink: docs/setting-up-gitlab-ci-for-ios-and-macos-projects/
keywords: cloud tutorials, cloud documentations, cloud instance, cloud hosting, mac server, mac mini m1, mac mini, macos server, mac, mac cloud, hosted mac, m1, apple m1, gitlab, ci, cicd, ci-cd, continous integration
---


{% anchor h2 %}
Gitlab CI - Overview
{% endanchor %}

GitLab's complete DevOps platform comes with built-in continuous integration (CI) and continuous delivery (CD) via [GitLab CI/CD](https://docs.gitlab.com/ee/ci/). A great solution to increase the productivity of developers and to motivate them to write higher-quality code without sacrificing speed. It runs a series of tests every time a commit is pushed, providing immediate visibility into the results of changes in the codebase. While it is not a hassle to setup a CI using Linux based machines, iOS and macOS developers may find it way more complicated to have access to a Mac that is connected and available 24 hours a day.

 In this tutorial, you will learn how to setup CI for iOS and macOS application development using a Scaleway Virtual Instance running the [GitLab application](https://www.scaleway.com/en/docs/install-gitlab-with-dbaas/) and a GitLab Runner that runs on a Scaleway hosted [Mac mini M1](https://www.scaleway.com/en/hello-m1/). To complete this tutorial, we assume that you have some basic knowledge in creating Xcode and GitLab projects, as well as some experiences using a Terminal and git.

> **Requirements**
>
- You have an account and are logged into [console.scaleway.com](https://console.scaleway.com)
- You have [configured your SSH Key](https://www.scaleway.com/en/docs/configure-new-ssh-key/)
- You have a Virtual Instance running the GitLab InstantApp

> **Note:** We assume you have already deployed a Virtual Instance running the GitLab InstantApp. If not, [deploy GitLab](https://www.scaleway.com/en/docs/install-gitlab-with-dbaas/) before continuing with this tutorial.


{% anchor h3 %}
Deploying the Mac mini M1
{% endanchor %}

1 . Log into your [Scaleway console](https://console.scaleway.com) and click on **Apple silicon** in the **Compute** section of it. 

2 . The Apple silicon M1 as-a-Service splash screen displays. Click Create a Mac mini M1.

3 . Enter the details for your Mac mini M1:

* Select the geographical region in which your Mac mini M1 will be deployed.
* Choose the macOS version you want to run on the Mac mini M1.
* Select the hardware configuration for your Mac mini M1.
* Enter a name for your Mac mini M1.

4 . Click Create a Mac mini M1 to launch the installation of your Apple silicon M1 as-a-Service.

5 . Once deployed click **VNC** from the Mac mini M1 overview page to launch the remote desktop client: 

<img src="/assets/images/docs/gitlab-ci/mac_vnc.png" width="700" alt="">

6 . Launch the **App Store** and install the **Xcode development environment** on your Mac mini M1:


{% anchor h3 %}
Setting-up the Homebrew package manager
{% endanchor %}

[Homebrew](https://brew.sh/) is a package manager for macOS. It can be used to manage the software installed on your Mac. We use it to install `gitlab-runner` on your Mac mini M1. 

1 . Open a new **Terminal** on your Mac by clicking on its icon: 

<img src="/assets/images/docs/gitlab-ci/mac_terminal.png" width="100" alt="">

2 . Copy-paste the following code in the terminal application and press **Enter** to install Homebrew and the Xcode command line tools: 

```sh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Leave the terminal window open, as it is required for the next step.

{% anchor h3 %}
Installing the Gitlab Runner
{% endanchor %}

The Gitlab Runner is an application installed on a computer different than your GitLab host, that run jobs in a pipeline. It executes the build task on your Mac mini M1 for the code you push to your GitLab instance. 

1 . Make sure you are still in the **Terminal** application. If you closed it after installing Homebrew, reopen a new one. 

2 . Run the following command to install `gitlab-runner`:

```
brew install gitlab-runner
```

{% anchor h3 %}
Configuring the Runner in Gitlab
{% endanchor %}

> **Note:** You require a Virtual Instance running the [GitLab InstantApp](https://www.scaleway.com/en/docs/how-to-use-the-gitlab-instant-apps/) for the following steps.

1 . Gitlab Runner requires a registration token for the link between your GitLab Instance and the Runner. Open the GitLab web interface of your Virtual Instance and log into it. 

2 . Select the project you want to use the GitLab with the Runner. If you don't have a project yet, click **+** > **Create Project** and fill in the required information about the project. 

3 . On the projects overview page, click **Settings** > **CI/CD** to view the Continuous Integration settings.

4 . On the Continuous Integration settings page, click **Expand**  in the **Runner** section to view the required information to link GitLab with your Runner:

<img src="/assets/images/docs/gitlab-ci/mac_runners.png" width="700" alt="">

5 . Scroll down to retrieve the Gitlab Instance URL and the registration token:

<img src="/assets/images/docs/gitlab-ci/mac_token.png" width="300" alt="">


6 . Run the following command in the Terminal application on your Mac to launch the configuration wizard for your GitLab Runner: 

```
gitlab-runner register
```

Enter the required information as follows
```
Runtime platform                                    arch=arm64 os=darwin pid=810 revision=2ebc4dc4 version=13.9.0
WARNING: Running in user-mode.                     
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...                   
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
http://163.172.141.212/   <- Enter the URL of your GitLab instance
Enter the registration token:
1mWBwzWAZSL7-pR18K3Y  <- Enter the registration token for your Runner
Enter a description for the runner:
[306a20a2-2e01-4f2e-bc76-a004d35d9962]: Mac mini M1  <- Enter a description for your Runner
Enter tags for the runner (comma-separated):
Mac, mini, M1, dev, xcode  <- Optionally, enter tags for the runner
Registering runner... succeeded                     runner=1mWBwzWA
Enter an executor: shell, virtualbox, docker+machine, custom, docker, docker-ssh, kubernetes, parallels, ssh, docker-ssh+machine:
shell  <- Enter the "shell" executor for the runner
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!  
```

7 . Reload the CI/CD configuration page of your GitLab instance. The runner is now linked to your project and displays as available:

<img src="/assets/images/docs/gitlab-ci/mac_runner_active.png" width="300" alt="">

> **Note:** If you have several projects in a GitLab group, you can configure the Runner on group-level. Runners available on group-level are available for all projects within this group. 

{% anchor h3 %}
Configuring CI for your project
{% endanchor %}

GitLab stores the configuration of the CI in a file called `.gitlab-ci.yml`. This file should be located within the folder you created for your project, this is typically the same directory where your Xcode project file (`ProjectName.xcodeproj`) is located. The GitLab CI configuration file is written in [YAML](https://yaml.org/).

Inside the configuration file you can specify information like: 
* The scripts you want to run.
Other configuration files and templates you want to include.
Dependencies and caches.
The commands you want to run in sequence and those you want to run in parallel.
The location to deploy your application to.
Whether you want to run the scripts automatically or trigger any of them manually.


1 . Open a text editor on your local computer and create the `.gitlab-ci.yml` file as in the following example. 

```
stages:
  - build
  - test

build-code-job:
  stage: build
  script:
    - echo "Check the ruby version, then build some Ruby project files:"
    - ruby -v
    - rake

test-code-job1:
  stage: test
  script:
    - echo "If the files are built successfully, test some files with one command:"
    - rake test1
```

2 . Save the file and make a new commit to add it to your repository. 

3 . Push the commit to GitLab. The CI will automatically launch the tasks on your Runner. 

For more information on the GitLab CI configuration file, refer to the [official documentation](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html).

{% anchor h3 %}
Conclusion
{% endanchor %}

Having a dedicated Mac available can save you time when you run the Continous Integration on your projects. In this tutorial you were able to setup a dedicated Mac mini M1 as Runner for a GitLab instance. If you want to learn more about the Mac mini M1 as-a-Service, refer to our [product documentation](https://www.scaleway.com/en/docs/apple-silicon-as-a-service-quickstart/).

<hr>
Mac mini, macOS are trademarks of Apple Inc., registered in the U.S. and other countries and regions. IOS is a trademark or registered trademark of Cisco in the U.S. and other countries and is used by Apple under license. Scaleway is not affiliated with Apple Inc.
